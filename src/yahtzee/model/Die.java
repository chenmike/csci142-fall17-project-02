package yahtzee.model;

import java.util.Random;
/**
 * This is the die class for the yahtzee game. This class
 * determines properties of individual die.
 * 
 * @author Michael Chen
 * @author Mikhail Maksimov 
 */
public class Die 
{
    public static final int NO_VALUE = -1;

    private int myFaceValue;
    private Random myRandom;
    private boolean myIsRolled;
    
    
    /**
     * constructor initializes properties of the die
     */
    public Die()
    {
    	myFaceValue = NO_VALUE;
    	myRandom = new Random();
    	myIsRolled = false;
    }
    
    
    /**
     * This method gets a random value from 
     * 1 to 6 for die toss when rolled
     * 
     * @return the result of the roll
     */
    public int roll()
    {
    	myFaceValue = myRandom.nextInt(6) + 1;
    	myIsRolled = true;
    	return myFaceValue;
    }
    
    
    /**
     * Chooses the die to be rolled or 
     * not to be rolled
     */
    public void toggleIsChosen()
    {
    	if (myIsRolled == true)
    	{
    		myIsRolled = false;
    	}
    	if (myIsRolled == false)
    	{
    		myIsRolled = true;
    	}
    }
    
    
    /**
     * Gets if the die has been rolled or not
     * 
     * @return myIsRolled if the die has been rolled or not
     */
    public boolean getIsRolled()
    {
        return myIsRolled;
    }
    
    
    /**
     * Returns true or false depending on if the die should
     * be rolled or not.
     * 
     * @return myIsRolled if the die is chosen to be rolled
     * or not
     */
	public boolean getIsChosen() 
	{
		return myIsRolled;
	}
    
    
    /**
     * Gets the face value of the die
     * 
     * @return myFaceValue the face value of the die
     */
    public int getFaceValue()
    {
        return myFaceValue;
    }
    
    
    /**
     * Sets the die to the state of not rolled
     */
    public void setNotRolled()
    {
    	myIsRolled=false;
    }
    
    
    /**
     * Shows information of die
     * 
     * @return string that shows face value of die
     */
    public String toString()
    {
        return "face: " + myFaceValue+"	Rolled: "+myIsRolled;
    }
    
    
    /**
     * Creates a clone of the die
     * 
     * @return copy a clone of original die 
     * that has same properties
     */
    public Object clone()
    {
    	Die copy;
    	copy = new Die();
    	copy.myFaceValue = myFaceValue;
    	copy.myIsRolled = myIsRolled;
    	return copy;
    }
}
