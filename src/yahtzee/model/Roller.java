package yahtzee.model;

import java.util.Arrays;
import java.util.Vector;

/**
 * This is the roller class for the yahtzee game. It rolls
 * the dice and records the dice values.
 * 
 * @author Mikhail Maksimov 
 * @author Michael Chen
 */
public class Roller
{
    private Vector<Die> myDice;
    private int myNumberOfDice;
    
    
    /**
     * Constructor for the Roller with the input of the number of dice.
     * It initializes the vector of dice and fills it with dice.
     * 
     * @param numDice Max number of dice that can be rolled
     */
    public Roller(int numDice)
    {
    	myNumberOfDice = numDice;
    	myDice = new Vector<Die>(numDice);
    	for (int i = 0; i < numDice; i++)
    	{
    		myDice.add(new Die());
    	}
    }
    
    
    /**
     * Returns the vector for the dice.
     * 
     * @return myDice Vector for storing dice values
     */
    public Vector<Die> getDice()
    {
        return myDice;
    }

    
    /**
     * Stores the dice values in an array.
     * 
     * @return diceValues Array of dice values
     */
    public int[] getDiceValues()
    {
    	int[] diceValues = new int [myNumberOfDice];
    	for (int i = 0; i < diceValues.length; i++)
    	{
    		Die die = myDice.get(i);
    		int value = die.getFaceValue();;
    		diceValues[i] = value;
    	}
        return diceValues;
    }

    
    /**
     * Rolls all of the dice to get values between 1 and 6.
     */
    public void roll()
    {
    	boolean[] toRoll = new boolean [myNumberOfDice];
    	Arrays.fill(toRoll, Boolean.TRUE);
    	rollSome(toRoll);
    }
    
    
    /**
     * Rolls only some of the dice based on which dice are
     * selected to roll.
     * 
     * @param toRoll Array of booleans that dictate which dice will be rolled
     */
    public void rollSome(boolean[] toRoll)
    {
    	for (int i = 0; i < myDice.size(); i++)
    	{
    		if (toRoll[i] == true)
    		{
    			Die die = myDice.get(i);
        		die.roll();
    		}
    	}
    }
    
    
    /**
     * Returns number of dice used for the yahtzee game. Usually 5 dice.
     * 
     * @return myNumberOfDice
     */
    public int getNumberOfDice()
    {
        return myNumberOfDice;
    }
    
    
    /**
     * Class for printing out a string when the name of a Roller object
     * is put into a println function.
     * 
     * @return String What I want it to say
     */
    public String toString()
    {
        return "number of dice: " + myNumberOfDice;
    
	}
}
