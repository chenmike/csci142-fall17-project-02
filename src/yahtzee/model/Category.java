package yahtzee.model;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Catagory class for the yahtzee game. 
 * 
 * @author Mikhail Maksimov
 * @author Michael Chen
 */
public class Category
{
    public static final int NO_VALUE = 0;

    private CategoryType myType;
    private String myName;
    private int myValue;
    private boolean myIsFilled;
    
    /**
     * Default constructor for Category objects
     * 
     * @param type The type of category used ex: ONES, TWOS etc.
     */
    public Category(CategoryType type)
    {
    	myType = type;
    	myValue = NO_VALUE;
    	if(type != null)
    	{
    		myName = type.getName();
    	}
    }

    /**
     * Fills the category for the right type. Takes the type and 
     * values of dice to calculate the score for the category
     * 
     * @param nums The integer array of face values of the dice
     * @return True if the category can be filled or false if it cannot be filled
     */
    public boolean fillCategoryValue(int[] nums)
    {
    	if(myType!=null)
    	{	
    		if(myType == CategoryType.ONES)
    		{
    			for(int i=0; i<nums.length; i++)
   	    		{
    				if(nums[i]==1)
    				{
    					myValue++;
    				}
   	    		}
   			}
    		if(myType == CategoryType.TWOS)
    		{
   				for(int i=0; i<nums.length; i++)
   	    		{
    				if(nums[i]==2)
	    			{
	    				myValue=myValue+2;
	   				}
        		}
    		}
    		if(myType == CategoryType.THREES)
    		{
    			for(int i=0; i<nums.length; i++)
        		{
    				if(nums[i]==3)
	    			{
	    				myValue=myValue+3;
	    			}
        		}
   			}
    		if(myType == CategoryType.FOURS)
    		{
    			for(int i=0; i<nums.length; i++)
    	    	{
	    			if(nums[i]==4)
	    			{
	    				myValue=myValue+4;
	    			}
    	   		}
    		}
    		if(myType == CategoryType.FIVES)
    		{
    			for(int i=0; i<nums.length; i++)
        		{
	    			if(nums[i]==5)
	    			{
	    				myValue=myValue+5;
	    			}
    	   		}
    		}
    		if(myType == CategoryType.SIXES)
    		{
   				for(int i=0; i<nums.length; i++)
   				{
   					if(nums[i]==6)
   					{
   						myValue=myValue+6;
   					}
   				}
    		}
    		if(myType == CategoryType.THREE_OF_KIND)
    		{
    			Arrays.sort(nums);
    			int check_1 = nums[0];
    			int check_2 = nums[1];
    			int check_3 = nums[2];
    			int check_4 = nums[3];
    			int check_5 = nums[4];
    			
    			if(check_1==check_2&&check_2==check_3)
    			{
    				myValue = IntStream.of(nums).sum();
    			}
    			if(check_2==check_3&&check_3==check_4)
    			{
    				myValue = IntStream.of(nums).sum();
    			}
    			if(check_3==check_4&&check_4==check_5)
    			{
    				myValue = IntStream.of(nums).sum();
    			}
    		}
    		
    		if(myType == CategoryType.FOUR_OF_KIND)
    		{
    			Arrays.sort(nums);
    			int check_1 = nums[0];
    			int check_2 = nums[1];
    			int check_3 = nums[2];
    			int check_4 = nums[3];
    			int check_5 = nums[4];
    			
    			if(check_1==check_2&&check_2==check_3&&check_3==check_4)
    			{
    				myValue = IntStream.of(nums).sum();
    			}
    			if(check_2==check_3&&check_3==check_4&&check_4==check_5)
    			{
    				myValue = IntStream.of(nums).sum();
    			}
    		}
    		
    		if(myType == CategoryType.FULL_HOUSE)
    		{
    			Arrays.sort(nums);
    			int check_1 = nums[0];
    			int check_2 = nums[1];
    			int check_3 = nums[2];
    			int check_4 = nums[3];
    			int check_5 = nums[4];
    			
    			if(check_1==check_2&&check_3==check_4&&check_4==check_5)
    			{
    				myValue = 25;
    			}
    			if(check_1==check_2&&check_2==check_3&&check_4==check_5)
    			{
    				myValue = 25;
    			}
    		}
    		
    		if (myType == CategoryType.SMALL_STRAIGHT)
    		{
    			int check_1=0;
    			int check_2=0;
    			int check_3=0;
    			int check_4=0;
    			int check_5=0;
    			int check_6=0;
    			
    			for(int i=0; i<nums.length; i++)
    			{
    				if(nums[i]==1)
    				{
    					check_1++;
    				}
    				if(nums[i]==2)
    				{
    					check_2++;
    				}
    				if(nums[i]==3)
    				{
    					check_3++;
    				}
    				if(nums[i]==4)
    				{
    					check_4++;
    				}
    				if(nums[i]==5)
    				{
    					check_5++;
    				}
    				if(nums[i]==6)
    				{
    					check_6++;
    				}
    			}
    			
    			if(check_1>=1&&check_2>=1&&check_3>=1&&check_4>=1)
    			{
    				myValue = 30;
    			}
    			if(check_2>=1&&check_3>=1&&check_4>=1&&check_5>=1)
    			{
    				myValue = 30;
    			}
    			if(check_3>=1&&check_4>=1&&check_5>=1&&check_6>=1)
    			{
    				myValue = 30;
    			}
    		}
    		
    		if (myType == CategoryType.LARGE_STRAIGHT)
    		{
    			Arrays.sort(nums);
    			int check_1 = nums[0];
    			int check_2 = nums[1];
    			int check_3 = nums[2];
    			int check_4 = nums[3];
    			int check_5 = nums[4];
    			
    			if(check_1==1&&check_2==2&&check_3==3&&check_4==4&&check_5==5)
    			{
    				myValue = 40;
    			}
    			if(check_1==2&&check_2==3&&check_3==4&&check_4==5&&check_5==6)
    			{
    				myValue = 40;
    			}
    		}
    		
    		if (myType == CategoryType.YAHTZEE)
    		{
    			Arrays.sort(nums);
    			int check_1 = nums[0];
    			int check_2 = nums[1];
    			int check_3 = nums[2];
    			int check_4 = nums[3];
    			int check_5 = nums[4];
    			
    			if(check_1==check_2&&check_2==check_3&&check_3==check_4&&check_4==check_5)
    			{
    				myValue = 50;
    			}
    		}
    		
    		if (myType == CategoryType.CHANCE) 
    		{
    			
    			for(int i=0; i<nums.length; i++)
    			{
    				myValue+=nums[i];
    			}
    		}
    	}
    		
    	for(int i=0; i<nums.length; i++) //checks if dice values are in range
    	{
    		if(nums[i]<1||nums[i]>6)
   			{
    			myValue=0;
    		}
    	}
    	if(myIsFilled==true)
    	{
    		myIsFilled=false;
    		return false;
    	}
    	myIsFilled=true;
    	return true;
    }
    
    /**
     * Return if the category has been filled or not
     * 
     * @return myIsFilled True if the category has been filled
     * and false if not
     */
    public boolean getIsFilled()
    {
        return myIsFilled;
    }
    
    /**
     * Gets the value of the category object
     * 
     * @return myValue Value of the category
     */
    public int getValue()
    {
        return myValue;
    }
    
    /**
     * Gets the name of the category object
     * 
     * @return myName The category name
     */
    public String getName()
    {
        return myName;
    }
    
    /**
     * Gets the type of the category object
     * 
     * @return myType Type of category
     */
    public CategoryType getType()
    {
        return myType;
    }
    
    /**
     * Prints a string of useful information when the name
     * of the category is put into a println function;
     * 
     * @return useful string of information
     */
    public String toString()
    {
        return "type: " + myType + "		" + "value: " + myValue + "	" + "filled: " + myIsFilled;
    }
    
    /**
     * Clones a category object with the same characteristics.
     * 
     * @return copy Clone of the original category object
     */
    public Object clone()
    {
        Category copy;
        copy = new Category(this.getType());
    	copy.myName = this.getName();
    	copy.myIsFilled = this.myIsFilled;
    	copy.myType = this.getType();
    	copy.myValue = this.getValue();
    	return copy;
    }
}
