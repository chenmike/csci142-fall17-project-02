package yahtzee.model;

/**
 * Class for the computer player AI
 * Not implemented yet
 * 
 * @author Mikhail Maksimov
 * @author Michael Chen
 */

public class ComputerPlayer extends Player
{
    public static boolean rollAgain(int[] values)
    {
        return false;
    }

    public static boolean[] selectDiceToReroll(int[] values)
    {
        return null;
    }

    public static CategoryType selectCategoryToFill(int[] values)
    {
        return null;
    }
}
